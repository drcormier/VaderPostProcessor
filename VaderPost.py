# convert motion with extruder to motion with PRIO commands.
from ..Script import Script
import re

class VaderPost(Script):
    def __init__(self):
        super().__init__()

    def getSettingDataString(self):
        return """{
            "name":"Vader Post Processor",
            "key": "VaderPost",
            "metadata": {},
            "version": 2,
            "settings":
            {
                "WorkOffset":
                {
                    "label": "Work Offset",
                    "description": "This work offset is called at the start of the script.",
                    "type": "str",
                    "default_value": "G54"
                },
                "Density":
                {
                    "label": "Density",
                    "description": "Density of print material.",
                    "unit": "kg/m^3",
                    "type": "float",
                    "default_value": 2690,
                    "minimum_value": 0.001
                },
                "AcceleratedLines":
                {
                    "label": "Accelerated Lines",
                    "description": "Maintain constant velocity and correct for drop travel timing while firing jet. Produces more accurate parts but takes longer.",
                    "type": "bool",
                    "default_value": true
                },
                "Acceleration":
                {
                    "label": "Acceleration",
                    "description": "Path acceleration.",
                    "unit": "m/s^2",
                    "type": "float",
                    "default_value": 10,
                    "minimum_value": 1,
                    "maximum_value": 10,
                    "enabled": "AcceleratedLines == True"
                },
                "DropDelay":
                {
                    "label": "Drop Delay",
                    "description": "Time delay from commanded pulse to drop impact.",
                    "unit": "s",
                    "type": "float",
                    "default_value": -0.002,
                    "enabled": "AcceleratedLines == True"
                },
                "SupportOptions":
                {
                    "label": "Modified Support Options",
                    "description": "Allows for modifying the pulse distance and f value for support",
                    "type": "bool",
                    "default_value": "false"
                },
                "SPDSelect":
                {
                    "label": "Support Pulse Distance Selector",
                    "description": "Selections include adding an offset or inputting an absolute value for the support pulse distance",
                    "type": "enum",
                    "options" : {"none":"None", "arb":"Arbitrary", "off":"Offset"},
                    "default_value": "none",
                    "enabled": "SupportOptions == True"
                },
                "SPDOffset":
                {
                    "label": "Support Pulse Distance Offset",
                    "description": "Offset for pulse distance that is active when support is printing.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 0,
                    "enabled": "SPDSelect == 'off' and SupportOptions == True"
                },
                "SPDVal":
                {
                    "label": "Support Pulse Distance Value",
                    "description": "Value for pulse distance that is active when support is printing.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 0.05,
                    "enabled": "SPDSelect == 'arb' and SupportOptions == True"
                },
                "PDAbs":
                {
                    "label": "Arbitrary Pulse Distance Value",
                    "description": "Change pulse distance to a specific value when printing non-support material, instead of using the formula",
                    "type": "bool",
                    "default_value": false,
                    "enabled": "SupportOptions == True"
                },
                "PDVal":
                {
                    "label": "Pulse Distance Value",
                    "description": "Value for pulse distance that is active when non-support material is printing.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 0.05,
                    "enabled": "PDAbs == True and SupportOptions == True"
                },
                "SFSelect":
                {
                    "label": "Support F Selector",
                    "description": "Selections include adding an offset or inputting an absolute value for the support F value",
                    "type": "enum",
                    "options" : {"none":"None", "arb":"Arbitrary", "off":"Offset"},
                    "default_value": "none",
                    "enabled":"SupportOptions == True"
                },
                "SFOffset":
                {
                    "label": "SF Offset",
                    "description": "Offset for F value (velocity) for support.",
                    "unit": "mm/s",
                    "type": "float",
                    "default_value": 0,
                    "enabled": "SFSelect == 'off' and SupportOptions == True"
                },
                "SFVal":
                {
                    "label": "Support F Value",
                    "description": "F value that is active when support is printing.",
                    "unit": "mm/s",
                    "type": "float",
                    "default_value": 150,
                    "enabled": "SFSelect == 'arb' and SupportOptions == True"
                },
                "InterfaceF":
                {
                    "label": "Interface F value",
                    "description": "F value that the slicer is using for printing the interface.",
                    "unit": "mm/s",
                    "type": "float",
                    "default_value": 10
                },
                "InterfaceZOffset":
                {
                    "label": "Interface Z Offset",
                    "description": "Z offset applied when printing the interface.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 10
                }
            }
        }"""

    def execute(self, data):
        outdata = ['G64\n',
                   self.getSettingValueByKey("WorkOffset")+'\n',
                   'OVRRAP=30\n',
                   'PRIO_PULSE_DISTANCE=CALIBRATION_MASS/{:.4f}/TRACK_AREA\n'.format(self.getSettingValueByKey("Density")/100),
                   'PRIO_ON_PATH_OFFSET=0.5*PRIO_PULSE_DISTANCE\n']
        if self.getSettingValueByKey("AcceleratedLines"):
            outdata.extend(['$SC_SD_MAX_PATH_ACCEL={}\n'.format(self.getSettingValueByKey("Acceleration")),
                            '$SC_IS_SD_MAX_PATH_ACCEL=TRUE\n'])
        else:
            outdata.append("$SC_IS_SD_MAX_PATH_ACCEL=FALSE\n")
        if not self.getSettingValueByKey("SupportOptions"):
            outdata.append("; WARNING: support settings turned off")
        EPSILON = 0.01
        interface_f = self.getSettingValueByKey("InterfaceF") * 60
        interface_z_offset = self.getSettingValueByKey("InterfaceZOffset")
        coord_names = ['X','Y','Z','E','F']
        coord = [0,0,0,0,9000]
        coord_prev = [0,0,0,0,0]
        e_move = False # should this G1 should be firing drops?
        e_move_prev = False
        first_z = True
        support = False
        PRIO_PULSE_DISTANCE = 'PRIO_PULSE_DISTANCE='
        reg_pulse_distance = 'CALIBRATION_MASS/{:.4f}/TRACK_AREA'.format(self.getSettingValueByKey("Density")/100)
        if self.getSettingValueByKey("PDAbs") == True:
            reg_pulse_distance = '{:.4f}'.format(self.getSettingValueByKey("PDVal"))
        outline = ''
        for item in data:
            lines = item.split('\n')
            for line in lines:
                print(line)
                # save comments from original file
                if re.match(';.*', line):
                    outdata.append(line+'\n');
                # upcoming code is support
                if re.match(';TYPE:SUPPORT', line) and self.getSettingValueByKey("SupportOptions") is True:
                    support = True
                    if self.getSettingValueByKey("SPDSelect") == 'off':
                        outdata.append(PRIO_PULSE_DISTANCE + '(' +
                                reg_pulse_distance + ')+{:.4f}\n'
                                .format(self.getSettingValueByKey("SPDOffset")))
                    # absolute mode
                    elif self.getSettingValueByKey("SPDSelect") == 'arb':
                        outdata.append(PRIO_PULSE_DISTANCE + '{:.4f}\n'
                                .format(self.getSettingValueByKey("SPDVal")))
                    else:
                        outdata.append(PRIO_PULSE_DISTANCE +
                                reg_pulse_distance + "\n")
                # upcoming code is not support
                if re.match(';TYPE:(?!SUPPORT).*', line) and self.getSettingValueByKey("SupportOptions") is True:
                    support = False
                    outdata.append(PRIO_PULSE_DISTANCE + reg_pulse_distance+'\n')
                if re.match('G0{0,1}[01]',line): # G0 (G00) or G1 (G01)
                    if re.match('G0{0,1}1', line): # G1 (G01)
                        for i in range(len(coord_names)):
                            match = re.search('(?<='+coord_names[i]+')[\d\.]+',line) # find XYZEF with a number after it
                            if match:
                                coord[i] = float(match.group(0))
                            else:
                                coord[i] = coord_prev[i]
                            if i==3:
                                if coord[i] == coord_prev[i]:
                                    e_move = False
                                else:
                                    print('should be e_move')
                                    e_move = True
                                    if coord[2] != coord_prev[2]:
                                        e_move = False
                                        raise(Exception('Printing moves with Z travel not implemented'))
                    else: # G0
                        for i in [0,1,2]: # skip E and F
                            match = re.search('(?<='+coord_names[i]+')[\d\.]+',line) # find XYZEF with a number after it
                            if match:
                                coord[i] = float(match.group(0))
                            else:
                                coord[i] = coord_prev[i]
                        e_move = False
                    if coord[2] != coord_prev[2] or first_z: # if Z changes, rapid to that height
                        cur_z = coord[2]
                        if interface_f - EPSILON <= coord[4] <= interface_f + EPSILON:
                            cur_z += interface_z_offset
                        outdata.append('G1 Z{:.4f} F{:.4f}\n'.format(cur_z, coord[4]))
                        first_z = False
                    elif e_move:
                        print('emove')
                        if self.getSettingValueByKey('AcceleratedLines'):
                            fval = coord[4]/60
                            # if printing support, add offset to f if SFSelect is False OR change f to SFVal if SFSelect is True
                            if support is True and self.getSettingValueByKey("SupportOptions") is True:
                            # offset mode
                                if self.getSettingValueByKey("SFSelect") == "off":
                                    fval = fval + self.getSettingValueByKey("SFOffset")
                            # absolute mode
                                elif self.getSettingValueByKey("SFSelect") == "arb":
                                    fval = self.getSettingValueByKey("SFVal")
                            if not (coord_prev[0] == coord[0] and coord_prev[1] == coord[1]):
                                outdata.append('CV_LINE({:.4f},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f})\n'.format(coord_prev[0],
                                                                                                     coord_prev[1],
                                                                                                     coord[0],
                                                                                                     coord[1],
                                                                                                     fval,
                                                                                                     self.getSettingValueByKey('DropDelay')))
                        else:
                            if coord[4] != coord_prev[4]: # if feedrate has changed
                                cur_z = coord[2]
                                if interface_f - EPSILON < coord[4] < interface_f + EPSILON:
                                    cur_z += interface_z_offset
                                outdata.append('G1 Z{:.4f} F{:.4f}\n'.format(cur_z, coord[4]))
                            outdata.append('LINE2D({:.4f},{:.4f},{:.4f},{:.4f})\n'.format(coord_prev[0],coord_prev[1],coord[0],coord[1]))
                    coord_prev = list(coord)
        outdata.append('M30\n') # end of program
        return outdata
